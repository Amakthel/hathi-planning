## Mission Statement
Hathi is a project under ICEI, and its purpose is to support ICEI's mission.
> *ICEI's mission is to support the development and stewardship of reliable, secure, and open source internet infrastructure software*

Hathi is to become the tool FOSS developers need in order to meet, discuss, collaborate and create and maintain the FOSS software of tomorrow.

#### Concept:
Hathi will provide a global network of discussion groups, easily searchable, with functionality to quickly give a good overview of in-depth, long-running and complicated technical discussions, free to join by anyone who wishes, revealing as little or as much about themselves as they wish to, and requiring as little hardware and bandwidth as possible.

Outside forces are likely to try to control, direct and censor the free expression of discussion participants; should they succceed, trust in the network will diminish and participation lowered, and collaboration efforts hindered.
Thus, Hathi will be designed to counter such attempts.

##### Priorities:
* Anonymity is essential
  * Participants must be safe from persecution even if they live within totalitarian regimes
  * In the so-called safe West, game developers are being harassed simply on account of having the wrong politics
  * Early warning of bugs in critical software infrastructure may sometimes only happen if the reporter is assured anonymity
* Encryption is essential
  * Discussions regarding zero-day exploits in critical software infrastructure need to be held discreetly
  * Without encryption, state agents and others could easily nullify participant anonymity
* Decentralization and federation are essential
  * Having single points of failure is not an option
  * Given the low ressources available, processing in the network needs to be spread out
  * Any given server may fail or become compromised; users must be able to migrate seamlessly

#### Goals:
1. Produce a protocol and reference implementations for a secure communications network
  1. The network must be robust in the face of censorship attempts
  2. The network must have high stability and work on unstable connections
  3. The network must support good hierarchical overviews such as threading
  4. The network must provide (directly or through extensions) modern social networking features
  5. Support use of identity servers; minimize logins, simplify user migration
2. Mentor and evaluate new volunteers
  1. Work with and mentor new ICEI software engineering volunteers
  2. Work with and mentor new ICEI staff
3. Outreach to developers and entrepreneurs in developing nations
  1. Support asynchronous technical collaboration
  2. Minimize hardware requirements of official Hathi implementations
  3. Assume work being done through intermittent network connections and mobile clients
4. Attract developers, visionaries and entrepreneurs
  1. Promote free choice in all things
  2. Be transparent and predictable
  3. Support self-governed interest groups
  4. Be minimalistic, utilitarian, and effective
  5. Present a well-documented, stable API for third-party interfaces.

#### Synergies:
Obviously a worldwide collaboration network will be useful for other groups than merely developers; designing Hathi to allow for the widest range of interests possible will have multiple benefits:
* The network will attract more interest  
  More interest means more participants, more networking, and more collaboration.
* Mixed needs will drive innovation  
  With a solid framework in place for them, various groups will make their own
  extensions to Hathi, giving a wide range of developers experience with Hathi and FOSS.
* Attractive to knowledge workers  
  Current media platforms are widely untrusted.  
  A guaranteed censorship-free network will attract knowledge workers.  
  Having knowledgeable ressources participate in the network on a regular basis will
  make it the go-to place for researching and finding in-depth discussions and
  knowledge in any given field.

Preliminary discussions of Hathi primary goals has resulted in a design concept which would accomodate a number of related use cases with little to zero overhead.

These use cases are:
* Non-profit collaboration
* Self-governed groups
* Coordination amongst minority groups
* Backbone to freedom of speech
* Alternative news outlet for people in repressive regimes
* Emergency peer to peer messaging

Supporting these use cases may not be the primary goal of ICEI, but they have broad appeal and encompass priorities and values of the FOSS community; accomodating potential future extensions and modules supporting these use cases in the Hathi design is therefore seen to be of significant strategic value with respect to creating and fostering FOSS communities of the future.