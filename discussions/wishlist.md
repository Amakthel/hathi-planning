
Goals
============================================================

------------------------------------------------------------------------

- Make it possible for like-minded individuals to self-organize and pool knowledge and resources in order to achieve their goals.

------------------------------------------------------------------------

Definitions
============================================================
- network: collection of interoperating servers
- group: collection of people; will probably often be a tribe
- topic: equivalent to thread? or equivalent to theme for a group?
- theme: taxonomy-based description for group so that people can find it by searching for specific subjects
- culture: collection of interoperating groups
- channel: specification of subject matter within a group or for an Actor; could be tag-based or a category? (Discord has channels in their servers)

------------------------------------------------------------------------

Features
============================================================

------------------------------------------------------------------------

#### Ability to send private messages between 2 users.

------------------------------------------------------------------------

#### Ability for all messages to be end-to-end encrypted.

------------------------------------------------------------------------

#### Choices of client applications

------------------------------------------------------------------------

#### Aunt Tillie compatibility or Aunt Tillie compatibility modes for all security and crypto features

------------------------------------------------------------------------

#### Want to be able to set up clients on several devices and have them work properly: no situations where I get a message on one device and then can't get it on another.

------------------------------------------------------------------------

#### Group messages

------------------------------------------------------------------------

#### Public messages (filterable by tags/taxonomies)

------------------------------------------------------------------------

#### Kanban overview

------------------------------------------------------------------------

#### Todo list overview

------------------------------------------------------------------------

#### Voting

------------------------------------------------------------------------

#### Collaborative editing of a document while being able to have a text chat going on the side

------------------------------------------------------------------------

#### simplistic git integration
- saving document versions and retrieving them
- hathi collaborative docs can then be pushed to git automatically
- which again can generate webpages automatically

------------------------------------------------------------------------

#### Filtering of content based on whatever: topics, network, groups, trust level, possibly keywords

------------------------------------------------------------------------

#### Persistent knowledge (wiki, repository?) seamlessly accumulated from ongoing threads and chats

------------------------------------------------------------------------

#### Money/bitcoin collection/escrow for group/tribe collaborations

------------------------------------------------------------------------

#### Trust network

------------------------------------------------------------------------

#### Easy hooking from and to other software; 'integrations'

------------------------------------------------------------------------

#### Key generation / storage Does the Right Thing

- This does not mean it is a black box
- ...What is this Right Thing?

------------------------------------------------------------------------

#### Support for bouncerless-IRC / Signal style communications

- Ie. group chat with preserved history?
- Secure? (what level of security?)

------------------------------------------------------------------------

Design
============================================================

------------------------------------------------------------------------

User Interface
------------------------------------------------------------

#### Combination of irc/chat and bulletin-boards/forums
- Method: Hierarchical threading
- Method: In-chat topic declarations autofiltered/sorted by clients

------------------------------------------------------------------------

#### Efficient and fast

------------------------------------------------------------------------

#### Separate, simple windows for window system to handle; possibly tabs that can be separated out like mozilla does it

------------------------------------------------------------------------

#### User-defined views of content; example given below ("View" example) but there's undoubtedly an actual standard for these things somewhere

------------------------------------------------------------------------

#### NO fancy formatting, graphical editing etc

------------------------------------------------------------------------

#### Grouping of topics into a structure

##### Example: Hierarchical threading

- Start topic inside a forum/channel.
- Users posts inside a topic.
- Ability to reply to a post within a topic in a threaded manner.

------------------------------------------------------------------------

#### Idea-Counterideas-Merging | Thesis-Antithesis-Synthesis | Discussions-Summaries

##### Example: Merging of ideas

= Channel
    * Some topic A
        - User A :: This is what I think about To
            - User B :: I agree with you
            - User C :: I don't know about this
        - User C :: This is my counter idea
            - User D :: I agree with you
            - User A :: I actually like your idea better
        - User B :: How about we merge both your ideas.

------------------------------------------------------------------------

#### User-defined window configuration

##### Example of a possible view configuration

1.  hsplit 30-70
    1.  "sidepanel" -> list: categories
    2.  vsplit 50-50
        1.  "topics" -> list: topics[category-selected]
        2.  title[topic-selected] -> show: message[topic-selected] 

------------------------------------------------------------------------
