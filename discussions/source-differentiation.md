Differentiation between sources so that clients can tell them apart
-------------------------------------------------------------------

#### Reasoning
Would prevent clients from attempting to reply to things that are
feeds from web sites and not other ActivityPub entities (i.e. a
person who will see their reply), reducing user confusion.

#### Handling
Hathi-server should return an error to clients that attempt to
direct message or otherwise interact with a feed besides
subscribing, unsubscribing, or mentioning.

#### Examples of possible sources

- output-only (e.g. public feed)  
  (Feeds are auto-generated from RSS/Atom sources (or ostatus),
    allowing users subscribe to e.g. a random blog without trouble)
- entity/person (will receive replies)
