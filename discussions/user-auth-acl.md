# User Authentication and ACL

This document is a proposed design of Hathi-social's user authentication and access control.

## Sequences

User authentication and ACL design is based on the following factors:

* Microservice-based architecture.
* Using public-key encryption.

The following sections provides an overview of the communication sequences for authorization and access control.

### User creation
![User Creation Diagram](user-auth-acl-diagrams/user_creation.png)

A user will use a Hathi Client to create a new account with the Hathi Server they wish to create an account in. During the registration, the user will decide whether to use (this implies the user will generate their own if needed) their own key pair or let Hathi manage the key pair.

For users choosing to manage their own keys, the user will provide the following:

* Username
* The public key encoded in a format for easy transport.

For users choosing to let Hati manage their keys, the user will provide the following:

* Username
* Password to encrypt the private key

### Authorization
![Authentication Diagram](user-auth-acl-diagrams/auth_sequence.png)

During user authorization the self-managed key user provides the username and private key to a Hathi client; if the private key is encrypted the Hathi Client will prompt the user for the password. For a hathi-managed key, the user provides the username and the private key password. The Hathi Client will ask the Hathi server for the user's encrypted private key.

The Hathi client will initiate user authentication with the Hathi server. The Hathi Server will send down a nonce encrypted with the user's public key. This will be sent back by the Hathi client after re-encrypting the decrypted nonce with the user's private key. If the Hathi Server determines the nonce is correct it will send the success response and the session token; otherwise the server will respond with authentication failed.

### Access control
![Request with ACL](user-auth-acl-diagrams/request_with_acl.png)

Once a user has been authenticated and a Hathi client is in possession of a session token, requests by the client should include this token to allow the server to determine whether the user has access to the object being requested. If the user associated to the token is authorized, the server will respond with the status of processing the user's requested action. Otherwise, the server will respond with a reauthenticate response. The client should ask the user to login again and initiate the authentication sequence.

For example, if a user wishes to post a note to their outbox the server will verify if the user associated with the session token is authorized to create notes on the outbox. If the user is authorized, the server will respond with the status of processing the note creation. Otherwise, the server will respond with reauthenticate. The client will display the login screen and initiate an authenticate to get a new session token. The previous action can be tried once a new token is available

### User migration
![User Migration](user-auth-acl-diagrams/user_migration.png)

If a user chooses to migrate their account from one server to another, the user specifies the new instance to migrate to. The user migration will go in three phases: data transfer, data import to the new server, redirect references from the old instance to the new instance.

In the data transfer phase the old instance should send the following information to the new instance:

* username in cleartext
* public key and other information about the user encrypted with the user's public key.

Once the transmission is complete, the old instance will notify the user to login to the new instance.

The data import state will hapen on the new instance. The user will login to the new instance for the purpose of making the private key available to decrypt the data received from the old instance.

Once the data has been decrypted, the new instance should iniate the handshake sequence of user authentication to verify that the public key has been transmitted successfully. The new instance will then store the information based on the user's key management type. At this point, the user should be considered authenticated and the data import phase complete. The new instance should notify the old instance to redirect queries about the user to it.

All objects created by the user should be left at the old instance where it was originally created.

## Design

This section provides design details for implementing user authentication and authorization.

### Credential storage

The server will store the following items:

* Username
* public key
* Other user-related information
* In the case of a hathi-managed key user, the encrypted private key. The private key should never be stored in cleartext

### Credential validation

Credential validation will be done by verifying that a nonce encrypted with the user's public key by the Hathi Server can be echoed back by the Hathi Client re-encrypted using the user's private key.

### Access control

Once the user has been authenticated and the Hathi client has a session token, all requests to the Hathi server should include the "session token". For every  request, the server should determine whether the request requires access control. The server must verify that all conditions are met before processing the request:

* The session token is active and associated to an active user.
* The user has the appropriate authorization to the object referenced by the UID.
* The user has the appropriate authorization to take the necessary action on the object.

If any of these conditions are not met the server will respond to indicate whether the user is not authorized or if a re-authorization is required.
