# Inter-process Communication

This document defines the inter-process communication protocol for Hathi modules

## Header data

### Binary / Text switch

The first byte of every packet.

Is either 'h' (0x68) for binary mode, or 'H' (0x48) for text mode.

### Transaction ID

Used to keep track of and order requests. A response packet uses the same
transaction ID as the request it is responding to.

### Session ID

The data blob used to track sessions. Null / all zeros means unauthenticated.

This is currently being represented by a 32 byte placeholder.

### Function ID / Name

The API function that is being called. Text mode *may* allow the function to be
specified by name instead of number.

### Data blob

The arguements specific to the selected function are here.


## Text mode

In text mode the header fields are lines in the format <field name>:<value>.
Each field is one line, with the exception of the data field, and version,
which the binary/text switch as its field name. Additionally all fields but
those two may be placed in any order. The switch / version field must be first,
and the data field must be last. The data field's value is the length of the
data after the newline. The count does not include the newlines before or after
the blob. Data *must* be terminated with a newline.

### Text mode example

    H:1
    transaction:23
    session:00112233445566778899AABBCCDDEEFF00112233445566778899AABBCCDDEEFF
    function:3
    data:48
    "foo"
    23
    {
      "@type":"Example",
      "bar":"quux"
    }

## Binary mode

In binary mode the fields are in a fixed order.

* protocol version number (uint8)

* transaction ID (uint16)

* session ID (whatever size is decided upon, should be a multiple of 4)

* function ID (uint8)

* 3 bytes padding / reserved to align with 4-byte word boundary.

* data length (uint32)

* data (zero padded at end to word boundary)

## Binary mode example

    68                             # 'h'
    01                             # version 1
    0017                           # transaction #23
    0011223344556677               # sesstion ID
    8899AABBCCDDEEFF
    0011223344556677
    8899AABBCCDDEEFF               # end session ID
    03000000                       # function 3
    00000064                       # data length
    03                             # type list (these are placeholder numbers)
    01
    04
    00                             # type list end
    00000003                       # string length
    foo                            # string
    00                             # string pad
    0000000000000017               # int 23
    0000001E                       # JSON length
    {"@type":"Example","bar"quux"} # JSON string
    0000                           # JSON pad

## Data

The data blob is structured. It consists of a list of unnamed fields of
predetermined datatypes.

### Types

* integer (int64)

* float (???)

WIP: don't know how to represent portably yet. Possibly revert to text here?

* string (length + padded data)

* JSON object (length + padded data)

The JSON object looks just like a string in the packet, but has different
parsing associated with it.

### Text mode types

In text mode each type starts on a new line. Types are distinguished on a type by type basis. All items are seperated by newlines.

* Strings start with a double quote

* Integers are numbers

* Floats are numbers with a decimal point

* JSON objects start with a curly bracket and end when the object's closing bracket happens.

### Binary mode types

Binary mode data blobs begin with a series of bytes, each byte is a type code specifying the type of data in that position. The type list ends with the first null byte. The list is padded with zeros to the 4-byte word boundary. After the type list comes the data, in the same order as the types.
