Technical Platform
==================

----------------------------------------------------------------------

Server
------

----------------------------------------------------------------------

### Language choice for server: Golang

Based on developer input on the matter, we had python and go as viable choices.  
Python is well-known and has good readability.  
Go, on the other hand, while less well-known, had an interest from developers and seemed to promise good efficiency since it's specialized for microservices.  
We tested go as server implementation language with the hathi server prototype, and it was deemed a good fit.  

----------------------------------------------------------------------

### Database backend: Sqlite vs postgresql

Consensus to attempt general support as much as possible;  
if choices have to be made, prioritize (decision was?) because (compelling
reason).

----------------------------------------------------------------------

Client
------

----------------------------------------------------------------------

### Language choice for client: Python3

Based on the fact that we will be wanting as many as possible to participate in client development, and also to provide a good, solid basis for alternative clients to be made.
Along with the other qualities of Python - but primarily its developer adoption and readability.

----------------------------------------------------------------------
