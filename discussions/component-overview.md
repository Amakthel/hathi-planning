Overview of Network Components
==============================

![Diagram](http://www.plantuml.com/plantuml/proxy?src=https://gitlab.com/hathi-social/planning/raw/master/discussions/component-overview.pump)

-----------------------------------------------------------------------

Components
----------

-----------------------------------------------------------------------

### User
A user is a person of flesh and blood, residing in the physical world,
using their device to access the network.  
This definition may seem obvious, but it is about to become important.

-----------------------------------------------------------------------

### Actors
An Actor is an entity specifically defined by ActivityPub as follows:  
> *Actor types are Object types that are capable of performing activities.*  
> *The core Actor Types include:*  
> - *Application*  
> - *Group*  
> - *Organization*  
> - *Person*  
> - *Service*  

As it can be seen, this concept is closer to being a datatype than it is
to being a real-world entity, but it does turn out to be quite useful in
many ways.  

#### Actor-Person
A basic user can be implemented as an Actor-Person.  
However, a single user can also be represented as any number of
Actor-Persons, e.g. one that faces the world as a website admin, one
which is more private and representing the person to family and friends,
plus one political profile.

#### Actor-Organization
In addition to this, we might have users representing organizations and
thus administering those organizations' Actor-Organization.

#### Actor-Service
Actor-Services can be many things (...)

-----------------------------------------------------------------------

### Hathi User Client
The User Client connects the user and lets them interact with the rest
of the network.
It is an application under the direct control of a user.

-----------------------------------------------------------------------

### Hathi Server
The Server is responsible for storage and transportation of objects and
messages in the network.  
This is all the Server does. Propagation of data. And temporary storage.

-----------------------------------------------------------------------

### Hathi Group Client
The Group Client is actually a daemon.
What it does is that it connects periodically to its Server, retrieves
the messages it might have, and then resends those messages to the
groups members.  
Importantly, this Group Client is completely independent of any Server
setup, and no user needs a public-facing ip address in order to run a
group.

-----------------------------------------------------------------------

### Hathi Service Client
A Service Client, like the Group Client, is actually a daemon.
And the Service Client, too, connects periodically to its Server, and
may retrieve, process and/or send messages.  
However, a Service Client is a broader concept.  
A Service Client can be almost anything.  
A news channel. An IFTTT interface. An IRC bridge. A voting system. A
bitcoin exchange.
The Hathi Development Team is creating an "availability service",
intended for coordination of multiple persons' availability for e.g.
meetings, across multiple time zones, and with fine-grained preferences
possible for the individual user.

-----------------------------------------------------------------------
