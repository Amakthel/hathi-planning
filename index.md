Structure
===========

- Root dir  
  Should only contain files which are important to give a good overview of the project.  
  This means  
  - README.md
  - architecture.md
  - designdoc-system.md
  - designdoc-server.md
  - designdoc-protocol.md
  - designdoc-web-client.md
  - designdoc-commandline-client.md
  - designdoc-group-daemon.md
- /discussions  
  - Discussions
  - Partial design specifications
  - Formal proposals
- /diagrams  
  - Diagrams
  - Graphicals overviews
  - Charts
- /todos
  - Lists of tasks  
    These would mostly be used to provide an overview of progress of subprojects which are too large and involved to casually explain
- /templates
  - templates for various things, eg. document structures


Ok
-------------------

README.md  
Ok  

/discussions/wishlist.md
Still seems relevant to have; invites for wishes, and can be revisited from time to time for planning, eg between phase changes.
Should be marked as fulfilled if such is the case.


Update
-------------------

architecture.md  
Needs to give a better overview.  
State problem, present priorities and concerns, and then with that context in hand, present the solution chosen. Possibly in-depth discussion of particularly thorny compromises.  


Move
---------------------

copyright-notice.txt -> templates  
great-refactor.md  -> todos  
*Moved to a better place*  

/discussions/roadmap.mediawiki
Roadmap for project.
Should be moved to root.
Needs some re-evaluation by team but is mostly current.
*Moved*


Design document partials
---------------------------

New files like this should be placed in /discussion.  
Publicly available but also not distracting the overview of rootdir.  

As they reach their conclusion, they should be incorporated into relevant design documents.  

*Several files moved from root to /discussions*

/discussions/database-api.md  
/discussions/validation-detection.md  
This looks like server design?  
In that case, it should be moved to a relevant section in the server design document.  

/discussions/interprocess-api.md  
/discussions/oauth-integration.md  
This looks like.. system design?  
In that case, it should be moved to a relevant section in the system design document.  
Did we not find a better solution than using oauth for authorization?  

/discussions/interprocess-comm.md  
This looks like.. protocol design?  
In that case, it should be moved to a relevant section in the protocol design document.  
If I'm not mistaken, interprocess-comm.md is in fact a packet structure specification.  

/discussions/identity_mgt_module.md  
This looks like a design document for the identity manager?  
If so:  

- it should be renamed to make this clear
- structure should be made to correspond with designdoc template

/discussions/datetime-stamps.md  
This should be part of the design doc for the module that makes use of this format  

/discussions/design-document.md
This was an earlier attempt at a design document.
Its contents should be incorporated into the newer structure.
Claims to superceed or incorporate

- wishlist.md
- technical-platform.md
- component-overview.md
- interservice-apis.md
- use-cases.md
- user-auth-acl.md

/client/rfc-hathiq.md
This looks like design of an ncurses-based client?
If so, its contents should late be incorporated to a designdoc-hathi-text-client.md
Reg. naming, even if the client/module itself gets a specific name, for the sake of clarity the design documents should reflect their placement in the overall system. Use designdoc-hathi-text-client-hathiq.md if necessary.

/discussions/blocking-features.md
This is a highlevel design partial; content might belong in system design doc or in server design doc.
Content could also be used for phase/milestone divided task list.

/discussions/component-overview.md
This is a highlevel design partial; content should be incorporated in either architecture.md or system design doc.
.pump diagram needs to be updated and re-generated as png

/discussions/db-strawman.md
This is just a single implementation note.
It relates to the database choice of the client.
It might be relevant for client design doc.
Otherwise should be deleted.

/discussions/delivery-scopes.md
This reflects decisions on message deliveries.
It should be incorporated into the system design doc.

/discussions/identity-handling.md
This is a discussion on how to handle identities in Hathi.
The contents should be incorporated into the design doc for the identity manager.

/discussions/indepth-evaluation-of-platforms.md
/discussions/proposal-identity-management-scuttlebot-A.md
/discussions/proposal-identity-management-zot-A.md
This is a comparison between Zot, Scuttlebot and ActivityPub.
It should probably be archived, but a summary should be incorporated into the system design document first.
Some renaming could be in order, too.
This is all a architecture discussion of which protocol to use, after all.

/discussions/requirements-list.md
This is a list and descriptions of features for the finished Hathi project.
Contents should probably be incorporated into the various design docs instead, and this file deleted when done.

/discussions/user-auth-acl.md
This document is a proposed design of Hathi-social's user authentication and access control.
The contents of this file should probably be moved into the system design document.

/discussions/router-design.md
/discussions/router-relay.md
Design of hathi-router.
Ought to be incorporated into design doc for router - structure from template, naming style of designdoc-hathi-router, and placed in root.

/discussions/technical-platform.md
This should probably be put into the system design document. Or perhaps to the individual modules' design documents.


Archived
-------------

Documents that are no longer up to date, but perhaps relevant to reference in future discussions.  

milestones.md  
This should be simply archived. It is no longer particularly relevant.  
Or perhaps it could be updated to reflect the roadman, only going more in-depth.  
That way, it could be used as an overview of progress.  
It would be more like a list of high-level tasks though.  
*Archived*

mission-statement.md  
problem-statement.md  
Archived or perhaps instead updated according to roadmap.  
*Archived*

/discussions/user-story-map.rst
This is an overview mapping phases of Hathi with various priorities.
It is a fine idea, but to maintain it up to date we'd need a kanban board or similar, and the use of it is limited considering the size of our group. The roadmap fulfills this purpose better.
Should be archived.
*Archived*


Delete
-------------

rfc-architecture.org  
rfc-architecture.md  
These were meant for discussion and are basically the old milestones/mission docs without anything new added to them.  
*Deleted*


Refactor
----------

/discussions/requirements-checklist.md
A comparison of our project to various other social platforms.
Still relevant.
Name is unclear though, should be renamed.
Or perhaps split into two, requirements(1) and platform comparison(2).
Contents might be useful as webpage.

/discussions/scenarios-personas.md
A very short file with semi-incomprehensible notes.
Lists various scenarios that we should take into account while writing or evaluating code.
Also ought to list various personas, but such a part has not been written.
It is relevant to have this information.
Should it be part of the architecture document?
Or some sort of project/process/evaluation document? Problem analysis?

/discussions/use-cases.md
These should be incorporated into a (the?) user-stories document.
User stories are and should be concerned with adding value to the user; the implementation is a result of this.
Extra description of the relevant user story, with specifications of what is needed for a complete implementation to fullfill (such as in this file), is a good thing.

/discussions/user-stories.org
This document contains the personas missing from scenarios-personas.
It also has various user stories, although the list is incomplete.
It has the problem of trying to do too many things at once.
It is trying to visualize the potential of the system.
It is trying to define what to do for each phase of Hathi, using user stories as subprojects to complete.
It should be divided into one document that goes all-in on exploring potentials; this can work as inspiration and possibly also as a webpage.
The user stories should be divided, not into the specific phases but rather wishlist, accepted, next phase and current phase.
This new file should also contain the system requirements that will be necessary to implement the user stories. And they should refer to each other.


Pending discussion
------------------------------------------------

/discussions/abuse-detection.md
/discussions/authorization.md
/discussions/content-filtering.md
/discussions/datahandling-features.md
/discussions/encrypted-propagation-layer.md
/discussions/interoperability.md 
/discussions/multifactor-authorization.md 
/discussions/search-features.md
/discussions/source-differentiation.md
/discussions/ssl-capability.md
/discussions/subscription-features.md
This looks like a discussion that never started.
Put contents into 'suggested discussions' for phase detail task list.

/discussions/fork_activipy.md
This is a proposal to fork the activipy project and make it part of the Hathi project.
There is no conclusion written to the proposal.
It should be adressed, concluded, contents transferred to a design doc if accepted, and archived.

/discussions/goals-and-values.md
These look quite relevant.
Perhaps they should be in the root dir.
Perhaps they should be a page on the website.
Propose to see if there is consensus.

/discussions/requirements-list-icn.md
List and description of features to implement for Internal Collaboration Network.
We should revisit this to
1) see if it needs updating
2) evaluate progress on ICN
